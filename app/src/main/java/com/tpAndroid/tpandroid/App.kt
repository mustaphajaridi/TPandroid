package com.tpAndroid.tpandroid;

import android.app.Application;
import com.tpAndroid.tpandroid.objectbox.ObjectBox

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        ObjectBox.init(this)
    }
}
