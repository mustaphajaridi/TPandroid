package com.tpAndroid.tpandroid.view.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.tpAndroid.tpandroid.databinding.FragmentStationsListBinding
import com.tpAndroid.tpandroid.model.Station
import com.tpAndroid.tpandroid.network.api.ApiClient
import com.tpAndroid.tpandroid.objectbox.ObjectBox
import com.tpAndroid.tpandroid.view.adapter.StationsAdapter
import com.tpAndroid.tpandroid.viewmodel.MainActivityViewModel
import com.tpAndroid.tpandroid.viewmodel.StationsListViewModel
import io.objectbox.Box
import kotlinx.coroutines.*


class StationsListFragment : Fragment() {

    companion object {
        private const val TAG = "sncf_stations_locator"
        private const val REQUEST_LOCATION_PERMISSIONS = 1
    }

    private val mainActivityViewModel by activityViewModels<MainActivityViewModel>()

    private val stationsListViewModel by viewModels<StationsListViewModel>()

    private lateinit var stationsAdapter: StationsAdapter
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private lateinit var binding: FragmentStationsListBinding

    private var stationsBox: Box<Station> = ObjectBox.get().boxFor(Station::class.java)


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Show action bar
        (activity as AppCompatActivity?)!!.supportActionBar!!.show()

        binding = FragmentStationsListBinding.inflate(inflater, container, false)
        val view = binding.root

        // Disable drawer
        mainActivityViewModel.setDrawerEnabled(true)

        mainActivityViewModel.setTitle("Gares proches")


        fusedLocationClient = LocationServices.getFusedLocationProviderClient(view.context)

        stationsListViewModel.isLoading.observe(viewLifecycleOwner){
            if(it){
                binding.stationsProgressBar.visibility = VISIBLE
                binding.rvStations.visibility = GONE
            }else{
                binding.stationsProgressBar.visibility = GONE
                binding.rvStations.visibility = VISIBLE
            }
        }

        val recoveredStations: MutableList<Station> = stationsBox.all
        stationsAdapter = StationsAdapter(recoveredStations)

        binding.rvStations.adapter = stationsAdapter


        binding.btnNearbyStations.setOnClickListener{
            if(!isPermissionsGranted()){
                requestPermissions()
            }else{
                getLastLocation()
            }
        }

        return view
    }

    private fun fetchAndSetNearestStations(userLocation: Location) {
        lifecycleScope.launch(Dispatchers.Main) {
            try {
                stationsListViewModel.setIsLoading(true)

                val response = ApiClient.apiService.getStations("${userLocation.latitude},${userLocation.longitude},10000")

                if (response.isSuccessful) {
                    showMessage("Gares recupérées avec succés")
                    stationsAdapter.setStations(userLocation, response.body()!!.records)
                } else {
                    showMessage("Error Occurred: ${response.message()}")
                    Log.e(TAG, response.message())
                }
            } catch (e: Exception) {
                showMessage("exception Occurred: ${e.message}")
                e.message?.let { Log.e(TAG, it) }
            }
            finally {
                stationsListViewModel.setIsLoading(false)
            }
        }
    }

    /**
     * Note: this method should be called after location permission has been granted.
     */
    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        fusedLocationClient.lastLocation
            .addOnCompleteListener(requireView().context as Activity) { task ->
                if (task.isSuccessful && task.result != null) {
                    val userLocation = task.result
                    fetchAndSetNearestStations(userLocation)
                } else {
                    Log.w(TAG, "getLastLocation:exception", task.exception)
                    showMessage("no_location_detected")
                }
            }
    }
    /**
     * Return the current state of the permissions needed.
     */
    private fun isPermissionsGranted(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(requireView().context,
            Manifest.permission.ACCESS_COARSE_LOCATION)
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissions() {
        Log.i(TAG, "Requesting permission")
        requestPermissions(
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION),
            REQUEST_LOCATION_PERMISSIONS
        )
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_LOCATION_PERMISSIONS) {
            if (grantResults.isEmpty()) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                showMessage("User interaction was cancelled")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation()
            } else {
                showMessage("Permission denied")
            }
        }else {
            showMessage("some Thing went wrong after requesting location")
            Log.w(TAG, "some Thing went wrong after requesting location")
        }
    }

    private fun showMessage(text: String) {
        Toast.makeText(requireContext(), text, Toast.LENGTH_LONG).show()
    }
}