package com.tpAndroid.tpandroid.view.fragment

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.tpAndroid.tpandroid.databinding.FragmentStationDetailsBinding
import com.tpAndroid.tpandroid.viewmodel.MainActivityViewModel
import com.tpAndroid.tpandroid.viewmodel.StationsListViewModel

class StationDetailsFragment : Fragment() {

    private lateinit var binding: FragmentStationDetailsBinding

    private val mainActivityViewModel by activityViewModels<MainActivityViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Show action bar
        binding = FragmentStationDetailsBinding.inflate(inflater,container,false)
        (activity as AppCompatActivity?)!!.supportActionBar!!.show()

        // Disable drawer
        mainActivityViewModel.setDrawerEnabled(false)

        mainActivityViewModel.setTitle("Gare")

        binding.libelle.text = arguments?.getString("libelle")
        binding.commune.text = arguments?.getString("commune")
        binding.departemen.text = arguments?.getString("departemen")
        binding.btnSeeInMaps.setOnClickListener{
            val latitude = arguments?.getString("latitude")
            val longitude = arguments?.getString("longitude")

            val uri = "google.navigation:q=$latitude,$longitude"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            intent.setPackage("com.google.android.apps.maps")
            startActivity(intent);
        }
        
        return binding.root
    }
}