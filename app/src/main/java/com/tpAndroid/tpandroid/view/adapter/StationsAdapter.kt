package com.tpAndroid.tpandroid.view.adapter

import android.location.Location
import android.location.Location.distanceBetween
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.tpAndroid.tpandroid.R
import com.tpAndroid.tpandroid.model.Station
import com.tpAndroid.tpandroid.objectbox.ObjectBox
import io.objectbox.Box
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class StationsAdapter(
    private val stations: MutableList<Station>
) : RecyclerView.Adapter<StationsAdapter.StationsViewHolder>() {

    private var stationsBox: Box<Station> = ObjectBox.get().boxFor(Station::class.java)

    class StationsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val stationLabel: TextView
        val distanceTo: TextView
        val btnDetails: ImageButton

        init {
            stationLabel = view.findViewById(R.id.station_label)
            distanceTo = view.findViewById(R.id.distance_to)
            btnDetails = view.findViewById(R.id.btn_details)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StationsViewHolder {
        return StationsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.station,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: StationsViewHolder, position: Int) {
        val currStation = stations[position]
        val distanceFromLocation = "%.2f".format(currStation.fields.distanceFromLocation?.div(1000))

        holder.apply {
            stationLabel.text = currStation.fields.libelle
            distanceTo.text = "$distanceFromLocation km"
            holder.itemView.setOnClickListener{ view -> goTostationDetails(currStation, view) }
        }
    }

    override fun getItemCount(): Int {
        return stations.size
    }

    fun setStations(userLocation: Location, receivedStations: MutableList<Station>){
        stations.clear()
        stationsBox.removeAll()

        val results = FloatArray(5)
        for (receivedStation in receivedStations){
            val receivedLatitude = receivedStation.fields.geo_point_2d[0]
            val receivedLongitude = receivedStation.fields.geo_point_2d[1]

            distanceBetween(userLocation.latitude, userLocation.longitude, receivedLatitude, receivedLongitude, results)

            receivedStation.fields.distanceFromLocation = results[0]

            stations.add(receivedStation)
        }

        stations.sortBy { it.fields.distanceFromLocation }

        GlobalScope.launch(Dispatchers.IO){
            stationsBox.put(stations)
        }


        notifyDataSetChanged()
    }

    private fun goTostationDetails(station: Station, view: View) {
        val bundle = Bundle().apply {
                putString("libelle", station.fields.libelle)
                putString("departemen", station.fields.departemen)
                putString("commune", station.fields.commune)
                putString("latitude", station.fields.geo_point_2d[0].toString())
                putString("longitude", station.fields.geo_point_2d[1].toString())
        }

        view.findNavController().navigate(R.id.action_stationsListFragment_to_stationDetailsFragment, bundle)
    }
}