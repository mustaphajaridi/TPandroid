package com.tpAndroid.tpandroid.view.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.android.material.navigation.NavigationView
import com.tpAndroid.tpandroid.R
import com.tpAndroid.tpandroid.application.showToast
import com.tpAndroid.tpandroid.databinding.ActivityMainBinding
import com.tpAndroid.tpandroid.viewmodel.MainActivityViewModel


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var binding: ActivityMainBinding

    private val mainViewModel by viewModels<MainActivityViewModel>()

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.mainAppBar.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        setupNavViewController()


        // Set page title
        mainViewModel.title.observe(this) {
            binding.mainAppBar.toolbarTitle.text = it
        }

        // Set drawer enabled
        mainViewModel.drawerEnabled.observe(this) {
            setDrawerEnabled(it)
        }
    }

    private fun updateToolbar(){
        val navController = findNavController(R.id.nav_host_fragment)

        binding.mainAppBar.toolbar.navigationIcon =
            if (navController.currentDestination == navController.graph.findNode(R.id.stationsListFragment))
                AppCompatResources.getDrawable(this, R.drawable.ic_menu)
            else
                AppCompatResources.getDrawable(this, R.drawable.ic_arrow_back)
    }

    private fun setupNavViewController(){

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        val navGraph = navController.navInflater.inflate(R.navigation.nav_graph)

        // for goBack button
        setupActionBarWithNavController(navController)

        val drawer = binding.drawerLayout

        // Setup appbar configuration
        appBarConfiguration = AppBarConfiguration(navGraph, drawer)

        val toggle = ActionBarDrawerToggle(this, drawer, binding.mainAppBar.toolbar, 0, 0)

        drawer.addDrawerListener(toggle)
        toggle.syncState()
        toggle.setToolbarNavigationClickListener{
            navController.popBackStack()
        }


        // Refresh toolbar style on fragment navigation
        supportFragmentManager.registerFragmentLifecycleCallbacks(object : FragmentManager.FragmentLifecycleCallbacks() {
            override fun onFragmentResumed(fm: FragmentManager, f: Fragment) {
                super.onFragmentResumed(fm, f)
                updateToolbar()
            }
        }, true)

        binding.navigationView.setNavigationItemSelectedListener(this)
    }

    /**
     * Enable or disable the drawer menu
     */
    private fun setDrawerEnabled(enabled: Boolean){
        val lockMode =
            if (enabled) DrawerLayout.LOCK_MODE_UNLOCKED else DrawerLayout.LOCK_MODE_LOCKED_CLOSED
        binding.drawerLayout.setDrawerLockMode(lockMode)
    }
    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.navigation_menu, menu)
        return true
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        val drawer = binding.drawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers()
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_camera -> { showToast("Camera is clicked") }
            R.id.nav_gallery -> { showToast("Gallery is clicked") }
            R.id.nav_slideshow -> { showToast("SlideShow is clicked") }
            R.id.nav_manage -> { showToast("Tools is clicked") }
            R.id.nav_share -> { showToast("Share is clicked") }
            R.id.nav_send -> { showToast("Send is clicked") }
        }

        binding.drawerLayout.closeDrawers()
        return true
    }
}