package com.tpAndroid.tpandroid.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainActivityViewModel: ViewModel() {

    private val titleLiveData = MutableLiveData("")
    var title = titleLiveData

    // Drawer enable state
    private val drawerEnabledLiveData = MutableLiveData<Boolean>()
    val drawerEnabled = drawerEnabledLiveData as LiveData<Boolean>


    fun setTitle(title: String){
        titleLiveData.value = title
    }

    fun setDrawerEnabled(enabled: Boolean){
        drawerEnabledLiveData.value = enabled
    }
}