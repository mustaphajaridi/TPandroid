package com.tpAndroid.tpandroid.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class StationsListViewModel: ViewModel() {

    private val isLoadingMutableLiveData = MutableLiveData(false)
    var isLoading = isLoadingMutableLiveData


    fun setIsLoading(status: Boolean){
        isLoadingMutableLiveData.value = status
    }
}