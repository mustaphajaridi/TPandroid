package com.tpAndroid.tpandroid.service

import com.tpAndroid.tpandroid.model.ResponseApi
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface StationsService {

    @GET("/api/records/1.0/search/?dataset=liste-des-gares&q=&rows=12&facet=fret&facet=voyageurs&facet=code_ligne&facet=departemen")
    suspend fun getStations(@Query("geofilter.distance") geoFilterDistance: String? = null): Response<ResponseApi>
}