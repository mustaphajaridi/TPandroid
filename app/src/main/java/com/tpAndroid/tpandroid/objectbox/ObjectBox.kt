package com.tpAndroid.tpandroid.objectbox

import android.content.Context
import com.tpAndroid.tpandroid.model.MyObjectBox
import io.objectbox.BoxStore

object ObjectBox {
    lateinit var store: BoxStore
        private set

    fun init(context: Context) {
        store = MyObjectBox.builder()
            .androidContext(context.applicationContext)
            .build()
    }

    fun get(): BoxStore {
        return store
    }
}