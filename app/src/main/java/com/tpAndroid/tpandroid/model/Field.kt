package com.tpAndroid.tpandroid.model

import androidx.annotation.Keep
import io.objectbox.annotation.Convert
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.converter.PropertyConverter
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.random.Random

@Keep
@Serializable
@Entity
data class Field (
    @Id
    var id: Long = Random.nextLong(),
    val commune: String = "",
    val libelle: String = "",
    val departemen: String = "",
    @Convert(converter = StringListConverter::class, dbType = String::class)
    val geo_point_2d: MutableList<Double> = mutableListOf(),
    var distanceFromLocation: Float?= null
)

class StringListConverter : PropertyConverter<MutableList<Double>?, String?> {
    override fun convertToEntityProperty(databaseValue: String?): MutableList<Double> {
        if (databaseValue == null) {
            return mutableListOf()
        }

        return Json.decodeFromString(databaseValue)
    }

    override fun convertToDatabaseValue(entityProperty: MutableList<Double>?): String {
        return Json.encodeToString(entityProperty)
    }
}


