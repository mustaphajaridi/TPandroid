package com.tpAndroid.tpandroid.model
import io.objectbox.annotation.Convert
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.converter.PropertyConverter
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.Serializable
import java.util.UUID
import kotlin.random.Random

@Entity
data class Station(
 @Id
 var id: Long = Random.nextLong(),
 val recordid: String = "${UUID(5,10)}",
 @Convert(converter = FieldConverter::class, dbType = String::class)
 val fields: Field
) : Serializable


class FieldConverter : PropertyConverter<Field?, String?> {
 override fun convertToEntityProperty(databaseValue: String?): Field? {
  if (databaseValue == null) {
   return null
  }

  return Json.decodeFromString<Field>(databaseValue)
 }

 override fun convertToDatabaseValue(entityProperty: Field?): String {
  return Json.encodeToString(entityProperty)
 }
}


