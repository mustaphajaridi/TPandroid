package com.tpAndroid.tpandroid.model
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import kotlin.random.Random

@Entity
data class ResponseApi(
    @Id
    var id: Long = Random.nextLong(),
    val records: MutableList<Station> = mutableListOf()
)